﻿using UnityEngine;

namespace ATT.Sprites
{
    [ExecuteInEditMode]
    public class SpriteOutline : MonoBehaviour
    {
        public Color color = Color.white;

        [Range(0, 16)]
        public int outlineSize = 1;

        private SpriteRenderer spriteRenderer;

        void OnEnable()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            UpdateOutline(true);
        }

        void OnDisable()
        {
            UpdateOutline(false);
        }

        void Update()
        {
            UpdateOutline(true);
        }

        void UpdateOutline(bool outline)
        {
            MaterialPropertyBlock mpb = new MaterialPropertyBlock();
            spriteRenderer.GetPropertyBlock(mpb);
            //mpb.SetFloat("_Outline_Size_1", outline ? 1f : 0);
            mpb.SetColor("_Outline_Color_1", color);
            mpb.SetFloat("_Outline_Size_1", outlineSize);
            spriteRenderer.SetPropertyBlock(mpb);
        }
    }
}