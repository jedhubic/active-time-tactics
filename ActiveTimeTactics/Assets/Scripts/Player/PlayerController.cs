﻿using ATT.GameInput;
using ATT.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Player
{
    public class PlayerController : MonoBehaviour
    {
        PlayerUnitManager _unitManager;

        private void Awake()
        {
            _unitManager = this.GetComponent<PlayerUnitManager>();
        }

        // Use this for initialization
        void Start()
        {

        }

        private void OnEnable()
        {
            BattleMapContext.OnConfirmInput += BattleMapContext_OnConfirmInput;
            BattleMapContext.OnCancelInput += BattleMapContext_OnCancelInput;
        }

       

        private void OnDisable()
        {
            BattleMapContext.OnConfirmInput -= BattleMapContext_OnConfirmInput;
            BattleMapContext.OnCancelInput -= BattleMapContext_OnCancelInput;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void BattleMapContext_OnConfirmInput(InputArgs inputArgs)
        {
        }

        private void BattleMapContext_OnCancelInput(InputArgs inputArgs)
        {
        }        
    }
}