﻿using ATT.GameInput;
using ATT.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ATT.Player
{
    public class PlayerUnitManager : MonoBehaviour
    {
        public List<UnitController> units;

        public UnitController selectedUnit { get { return _selectedUnit; } }

        UnitController _selectedUnit;

        UnitController _highlightedUnit;

        int _selectedUnitIndex = 0;

        // Use this for initialization
        void Start()
        {

        }

        void OnEnable()
        {
            BattleMapContext.OnConfirmInput += BattleMapContext_OnConfirmInput;
            BattleMapContext.OnCancelInput += BattleMapContext_OnCancelInput;
            BattleMapContext.OnNextInput += BattleMapContext_OnNextInput;
            BattleMapContext.OnPreviousInput += BattleMapContext_OnPreviousInput;
            PlayerCaret.OnToggleUnitHighlighted += OnToggleUnitHighlighted;
        }        

        void OnDisable()
        {
            BattleMapContext.OnConfirmInput -= BattleMapContext_OnConfirmInput;
            BattleMapContext.OnCancelInput -= BattleMapContext_OnCancelInput;
            BattleMapContext.OnNextInput -= BattleMapContext_OnNextInput;
            BattleMapContext.OnPreviousInput -= BattleMapContext_OnPreviousInput;
            PlayerCaret.OnToggleUnitHighlighted -= OnToggleUnitHighlighted;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void BattleMapContext_OnConfirmInput(InputArgs inputArgs)
        {            
            if (_highlightedUnit != null && _highlightedUnit != _selectedUnit)
            {
                if (_selectedUnit != null)
                {
                    _selectedUnit.ToggleSelectUnit(false);
                }

                _selectedUnit = _highlightedUnit;
                _selectedUnit.ToggleSelectUnit(true);
            }
        }

        private void BattleMapContext_OnCancelInput(InputArgs inputArgs)
        {
            DeselectUnit();
        }

        private void BattleMapContext_OnNextInput(InputArgs inputArgs)
        {
            CycleUnitSelection(_selectedUnitIndex + 1);
        }        

        private void BattleMapContext_OnPreviousInput(InputArgs inputArgs)
        {
            CycleUnitSelection(_selectedUnitIndex - 1);
        }

        private void OnToggleUnitHighlighted(bool isHighlighted, UnitController unitController)
        {
            if (isHighlighted)
            {
                if (_highlightedUnit != unitController)
                {
                    _highlightedUnit = unitController;
                    _highlightedUnit.ToggleHighlightUnit(true);
                }
            }
            else
            {
                if (_highlightedUnit != null && _highlightedUnit == unitController)
                {
                    _highlightedUnit.ToggleHighlightUnit(false);
                    _highlightedUnit = null;
                }                
            }
        }

        private void CycleUnitSelection(int newUnitIndex)
        {
            //if (_selectedUnit == null)
            //{
                //_selectedUnitIndex = 0;
            //}
            //else
            //{
                _selectedUnitIndex = newUnitIndex;
                if (newUnitIndex >= units.Count)
                {
                    _selectedUnitIndex = 0;
                }
                else if (newUnitIndex < 0)
                {
                    _selectedUnitIndex = units.Count - 1;
                }
            //}

            if (units.Count > 0)
            {
                for (int i = 0; i < units.Count; i++)
                {
                    units[i].ToggleSelectUnit(false);
                }

                _selectedUnit = units[_selectedUnitIndex];
                _selectedUnit.ToggleSelectUnit(true);
            }
        }

        private void SelectUnit()
        {

        }

        private void DeselectUnit()
        {
            if (_selectedUnit != null)
            {
                _selectedUnit.ToggleSelectUnit(false);
                _selectedUnit = null;
            }
        }
    }
}