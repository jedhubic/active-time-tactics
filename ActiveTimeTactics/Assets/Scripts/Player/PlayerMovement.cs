﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using ATT.GameInput;
using ATT.Units;
using DG.Tweening;

namespace ATT.Player
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerMovement : MonoBehaviour
    {
        public float movementSpeed;

        public float rotateSpeed;

        public float highlightMoveDuration;

        private Tweener _rotationTweener;

        private Vector2 _movementVector = Vector2.zero;

        private Tweener _movementTweener;

        void Awake()
        {
        }

        // Use this for initialization
        void Start()
        {            
        }

        void OnEnable()
        {
            BattleMapContext.OnRotateInput += BattleMapContext_OnRotateInput;
            BattleMapContext.OnMovementInputUpdated += BattleMapContext_OnMovementInputUpdated;
            UnitController.OnUnitSelected += OnUnitSelected;
            
        }       

        void OnDisable()
        {
            BattleMapContext.OnRotateInput -= BattleMapContext_OnRotateInput;
            BattleMapContext.OnMovementInputUpdated -= BattleMapContext_OnMovementInputUpdated;
            UnitController.OnUnitSelected -= OnUnitSelected;
        }

        // Update is called once per frame
        void Update()
        {
            if (_movementTweener == null || !_movementTweener.IsPlaying())
            {
                UpdateMovement();
            }            
        }       

        private void BattleMapContext_OnRotateInput(InputArgs inputArgs, float rotationDirection)
        {
            RotatePlayer(rotationDirection);
        }

        private void BattleMapContext_OnMovementInputUpdated(InputArgs inputArgs, Vector2 movementVector)
        {
            _movementVector = movementVector;
        }

        void RotatePlayer(float axisValue)
        {
            if (_rotationTweener == null || !_rotationTweener.IsPlaying())
            {
                Vector3 rotationVector = new Vector3(0f, 90f * axisValue, 0f);
                _rotationTweener = this.transform.DOLocalRotate(this.transform.localEulerAngles + rotationVector, rotateSpeed).SetEase(Ease.InSine);
            }
        }

        void UpdateMovement()
        {
            Vector3 movementVector = Vector3.zero;
            movementVector.x = _movementVector.x;
            movementVector.z = _movementVector.y;

            this.transform.Translate(movementVector * movementSpeed * Time.deltaTime);
        }

        void MoveForward(float axisValue)
        {

        }

        void MoveRight(float axisValue)
        {

        }

        void OnUnitSelected(UnitController unitController)
        {
            MoveToLocation(unitController.transform.position);
        }

        void MoveToLocation(Vector3 position)
        {
            Vector3 newPosition = position;
            position.y = this.transform.position.y;
            _movementTweener = this.transform.DOMove(newPosition, highlightMoveDuration);
        }
    }
}