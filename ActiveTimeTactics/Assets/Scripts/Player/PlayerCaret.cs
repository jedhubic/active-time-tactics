﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using ATT.Units;

namespace ATT.Player
{
    public class PlayerCaret : MonoBehaviour
    {
        public delegate void ToggleUnitHighlighted(bool isHighlighted, UnitController unitController);
        public static event ToggleUnitHighlighted OnToggleUnitHighlighted;

        public LayerMask layerMask;

        public Vector3 surfaceOffset;

        private Vector3 _initialRotation;

        private MeshRenderer _renderer;

        private Tweener _moveTweener;

        private void Awake()
        {
            _renderer = this.GetComponent<MeshRenderer>();
        }

        // Use this for initialization
        void Start()
        {
            _initialRotation = this.transform.eulerAngles;
        }

        // Update is called once per frame
        void Update()
        {
            this.transform.eulerAngles = _initialRotation;
            UpdateSurfaceOffset();
            UpdateUnitHighlighted();
        }

        void UpdateSurfaceOffset()
        {
            RaycastHit raycastHit;
            Vector3 raycastDirection = new Vector3(0f, -1f, 0f);
            Vector3 raycastPosition = this.transform.position;
            raycastPosition.y += 10;
            if (Physics.Raycast(raycastPosition, raycastDirection, out raycastHit, 100f, layerMask.value))
            {
                //this.transform.position = raycastHit.point + surfaceOffset;
                Vector3 newPosition = raycastHit.point + surfaceOffset;

                if (_moveTweener != null)
                {
                    _moveTweener.Kill();
                }

                if (this.transform.position.y != newPosition.y)
                {
                    this.transform.localPosition = new Vector3(0f, this.transform.localPosition.y, 0f);
                    _moveTweener = this.transform.DOMoveY(newPosition.y, 0.1f);
                }
                else
                {
                    this.transform.position = newPosition;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Unit")
            {
                _renderer.enabled = false;
                UnitController unitController = other.GetComponent<UnitController>();
                if (unitController)
                {
                    //unitController.ToggleHighlightUnit(true);
                    if (OnToggleUnitHighlighted != null)
                    {
                        OnToggleUnitHighlighted(true, unitController);
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Unit")
            {                
                _renderer.enabled = true;
                UnitController unitController = other.GetComponent<UnitController>();
                if (unitController)
                {
                    
                    //unitController.ToggleHighlightUnit(false);
                    if (OnToggleUnitHighlighted != null)
                    {
                        OnToggleUnitHighlighted(false, unitController);
                    }
                }
            }
        }

        void UpdateUnitHighlighted()
        {

        }
    }
}