﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Constants
{
    public enum ViewpointTypes
    {
        NONE = 0,
        FRONT = 1,
        BACK = 2,
        SIDE = 3,
        ANGLE_FRONT = 4,
        ANGLE_BACK = 5
    }
}