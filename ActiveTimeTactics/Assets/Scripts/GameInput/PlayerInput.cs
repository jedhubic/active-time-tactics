﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace ATT.GameInput
{
    public class PlayerInput : MonoBehaviour
    {
        public int playerId = 0;

        private Rewired.Player _rewiredPlayer;

        private IInputContext _inputContext;

        void Awake()
        {
            _rewiredPlayer = ReInput.players.GetPlayer(playerId);            
        }

        // Use this for initialization
        void Start()
        {
            //_rewiredPlayer.controllers.maps.ClearAllMaps(true);             
            _inputContext = this.gameObject.AddComponent<BattleMapContext>();
            _inputContext.playerId = playerId;
        }

        // Update is called once per frame
        void Update()
        {
        }      
    }
}