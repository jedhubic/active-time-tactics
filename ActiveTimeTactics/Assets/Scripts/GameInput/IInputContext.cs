﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.GameInput
{
    public interface IInputContext
    {
        int playerId { get; set; }

        void Remove();
    }
}