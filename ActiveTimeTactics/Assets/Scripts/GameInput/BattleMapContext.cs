﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace ATT.GameInput
{
    public class BattleMapContext : MonoBehaviour, IInputContext
    {
        public delegate void ButtonInput(InputArgs inputArgs);
        public static event ButtonInput OnConfirmInput;
        public static event ButtonInput OnCancelInput;
        public static event ButtonInput OnNextInput;
        public static event ButtonInput OnPreviousInput;

        public delegate void MovementInputUpdated(InputArgs inputArgs, Vector2 movementVector);
        public static event MovementInputUpdated OnMovementInputUpdated;

        public delegate void RotateInput(InputArgs inputArgs, float rotationDirection);
        public static event RotateInput OnRotateInput;

        public int playerId { get; set; }

        private Rewired.Player _rewiredPlayer;

        private Vector2 _movementVector;

        private InputArgs _baseInputArgs;
        
        // Use this for initialization
        void Start()
        {
            _rewiredPlayer = ReInput.players.GetPlayer(playerId);
            _baseInputArgs = new InputArgs() { playerId = playerId };
        }

        // Update is called once per frame
        void Update()
        {
            GetInput();
        }

        public void Remove()
        {
            Destroy(this);
        }

        public void GetInput()
        {
            Vector2 newMovementVector = new Vector2();
            newMovementVector.x = _rewiredPlayer.GetAxis("Move Horizontal");
            newMovementVector.y = _rewiredPlayer.GetAxis("Move Vertical");

            if (_movementVector != newMovementVector)
            {
                _movementVector = newMovementVector;
                if (OnMovementInputUpdated != null)
                {
                    OnMovementInputUpdated(_baseInputArgs, _movementVector);
                }
            }

            float rotationDirection = 0f;
            if (_rewiredPlayer.GetButtonDown("Rotate Right"))
            {
                rotationDirection = -1f;
            }
            else if (_rewiredPlayer.GetButtonDown("Rotate Left"))
            {
                rotationDirection = 1f;
            }

            if (rotationDirection != 0f)
            {
                if (OnRotateInput != null)
                {
                    OnRotateInput(_baseInputArgs, rotationDirection);
                }
            }

            if (_rewiredPlayer.GetButtonDown("Confirm"))
            {                
                if (OnConfirmInput != null)
                {
                    OnConfirmInput(_baseInputArgs);
                }
            }
            else if (_rewiredPlayer.GetButtonDown("Cancel"))
            {                
                if (OnCancelInput != null)
                {
                    OnCancelInput(_baseInputArgs);
                }
            }
            else if (_rewiredPlayer.GetButtonDown("Next"))
            {
                if (OnNextInput != null)
                {
                    OnNextInput(_baseInputArgs);
                }
            }
            else if (_rewiredPlayer.GetButtonDown("Previous"))
            {
                if (OnPreviousInput != null)
                {
                    OnPreviousInput(_baseInputArgs);
                }
            }
        }
    }
}