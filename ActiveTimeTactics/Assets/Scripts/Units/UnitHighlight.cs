﻿using ATT.Sprites;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Units
{
    public class UnitHighlight : MonoBehaviour
    {
        public Color highlightColor;

        public Color selectColor;

        private SpriteOutline _spriteOutline;

        private bool _isHighlighted = false;

        private bool _isSelected = false;

        private void Awake()
        {
            _spriteOutline = this.GetComponent<SpriteOutline>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ToggleHighlight(bool isHighlighted)
        {
            _isHighlighted = isHighlighted;

            SetHighlight(_isHighlighted, _isSelected);
        }

        public void ToggleSelection(bool isSelected)
        {
            _isSelected = isSelected;

            SetHighlight(_isHighlighted, _isSelected);
        }

        private void SetHighlight(bool isHighlighted, bool isSelected)
        {            
            _spriteOutline.outlineSize = 0;

            if (isSelected)
            {
                _spriteOutline.color = selectColor;
                _spriteOutline.outlineSize = 2;
            }
            else
            {
                if (isHighlighted)
                {
                    _spriteOutline.color = highlightColor;
                    _spriteOutline.outlineSize = 1;
                }
            }
        }
    }
}