﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Units
{
    public class BillboardToCam : MonoBehaviour
    {
        public Camera gameCamera;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 lookAtPos = gameCamera.transform.position;
            lookAtPos.y = this.transform.position.y;
            this.transform.LookAt(lookAtPos);
        }
    }
}