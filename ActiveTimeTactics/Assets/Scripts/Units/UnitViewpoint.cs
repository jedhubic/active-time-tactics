﻿using ATT.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Units
{
    public class UnitViewpoint : MonoBehaviour
    {
        public Camera gameCamera;

        private UnitVisual _unitVisual;

        private void Awake()
        {
            _unitVisual = this.GetComponentInChildren<UnitVisual>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            UpdateViewpoint();
        }

        void UpdateViewpoint()
        {
            ViewpointTypes viewpointType = ViewpointTypes.FRONT;

            float unitDot = Vector3.Dot(this.transform.forward, _unitVisual.transform.forward);
            float unitDotAbs = Mathf.Abs(unitDot);
            float unitCrossY = Vector3.Cross(this.transform.forward, _unitVisual.transform.forward).y;

            // First determine if camera is in front/behind, at an angle, or to the side of the player.
            if (unitDotAbs >= 0.9f && unitDotAbs <= 1.0f)
            {
                if (unitDot >= 0f)
                {
                    // In front
                    viewpointType = ViewpointTypes.FRONT;
                }
                else
                {
                    // Behind
                    viewpointType = ViewpointTypes.BACK;
                }
            }
            else if (unitDotAbs >= 0.33f && unitDotAbs < 0.9f)
            {
                if (unitDot >= 0f)
                {
                    // In front angled
                    viewpointType = ViewpointTypes.ANGLE_FRONT;
                }
                else
                {
                    // Behind angled
                    viewpointType = ViewpointTypes.ANGLE_BACK;
                }
            }
            else if (unitDot >= -0.33f && unitDot <= 0.33f)
            {
                // Side
                viewpointType = ViewpointTypes.SIDE;
            }

            bool isCameraRight = true;
            if (unitCrossY < 0f)
            {
                isCameraRight = false;
            }

            _unitVisual.SetSpriteByViewpoint(viewpointType, isCameraRight);
        }
    }
}