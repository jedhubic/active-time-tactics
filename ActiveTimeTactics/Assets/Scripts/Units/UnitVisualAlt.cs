﻿using ATT.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace ATT.Units
{
    [ExecuteInEditMode]
    public class UnitVisualAlt : MonoBehaviour
    {
        public SpriteAtlas spriteAtlas;

        public string characterName;

        public Camera gameCamera;

        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        // TODO: Look at using C# 7 tuples for the dictionary keys and actually storing the sprites as values.
        private Dictionary<ViewpointTypes, Sprite> _spriteViewpointMap;

        private ViewpointTypes _currentViewpoint = ViewpointTypes.NONE;

        private void Awake()
        {
            
        }

        // Use this for initialization
        void Start()
        {
            _spriteViewpointMap = GetViewpointSpriteMap();
            SetSpriteByViewpoint(ViewpointTypes.FRONT, true);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private Dictionary<ViewpointTypes, Sprite> GetViewpointSpriteMap()
        {
            Dictionary<ViewpointTypes, Sprite> spriteViewpointMap = new Dictionary<ViewpointTypes, Sprite>();

            // Get the character-specific sprites from the sprite atlas.
            Sprite[] spriteArray = new Sprite[spriteAtlas.spriteCount];
            spriteAtlas.GetSprites(spriteArray);

            spriteViewpointMap.Add(ViewpointTypes.FRONT, spriteAtlas.GetSprite($"{characterName}_front"));
            spriteViewpointMap.Add(ViewpointTypes.BACK, spriteAtlas.GetSprite($"{characterName}_back"));
            spriteViewpointMap.Add(ViewpointTypes.SIDE, spriteAtlas.GetSprite($"{characterName}_side"));
            spriteViewpointMap.Add(ViewpointTypes.ANGLE_FRONT, spriteAtlas.GetSprite($"{characterName}_frontAngle"));
            spriteViewpointMap.Add(ViewpointTypes.ANGLE_BACK, spriteAtlas.GetSprite($"{characterName}_backAngle"));

            return spriteViewpointMap;
        }

        public void SetSpriteByViewpoint(ViewpointTypes viewpointType, bool isCameraRightSide)
        {
            if (_currentViewpoint != viewpointType && _spriteViewpointMap[viewpointType])
            {
                _currentViewpoint = viewpointType;
                _spriteRenderer.sprite = _spriteViewpointMap[_currentViewpoint];
                _spriteRenderer.flipX = !isCameraRightSide;
            }
        }
    }
}