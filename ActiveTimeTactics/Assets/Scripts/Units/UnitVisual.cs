﻿using ATT.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace ATT.Units
{
    [ExecuteInEditMode]
    public class UnitVisual : MonoBehaviour
    {
        public SpriteAtlas spriteAtlas;

        public UnityEngine.Camera gameCamera;

        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        private Sprite[] _sprites;

        private Dictionary<ViewpointTypes, string> _spriteViewpointMap;

        private ViewpointTypes _currentViewpoint = ViewpointTypes.NONE;

        private void Awake()
        {
            // TODO: Look at using tuples for the dictionary keys and actually storing the sprites as values.
            _spriteViewpointMap = new Dictionary<ViewpointTypes, string>();
            _spriteViewpointMap.Add(ViewpointTypes.FRONT, "front");
            _spriteViewpointMap.Add(ViewpointTypes.BACK, "back");
            _spriteViewpointMap.Add(ViewpointTypes.SIDE, "side");
            _spriteViewpointMap.Add(ViewpointTypes.ANGLE_FRONT, "frontAngle");
            _spriteViewpointMap.Add(ViewpointTypes.ANGLE_BACK, "backAngle");
        }

        // Use this for initialization
        void Start()
        {
            _sprites = new Sprite[spriteAtlas.spriteCount];
            spriteAtlas.GetSprites(_sprites);

            SetSpriteByViewpoint(ViewpointTypes.FRONT, true);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetSpriteByViewpoint(ViewpointTypes viewpointType, bool isCameraRightSide)
        {
            if (_currentViewpoint != viewpointType && _spriteViewpointMap.ContainsKey(viewpointType))
            {
                _currentViewpoint = viewpointType;

                // TODO: Change this call to something more efficient so sprites aren't constantly being cloned.
                _spriteRenderer.sprite = spriteAtlas.GetSprite(_spriteViewpointMap[viewpointType]);

                _spriteRenderer.flipX = !isCameraRightSide;
            }
        }
    }
}