﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ATT.Units
{
    public class UnitController : MonoBehaviour
    {
        public delegate void UnitHighlighted(UnitController unitController);
        public static event UnitHighlighted OnUnitHighlighted;

        public delegate void UnitSelected(UnitController unitController);
        public static event UnitSelected OnUnitSelected;        

        private UnitHighlight _unitHighlight;

        private void Awake()
        {
            _unitHighlight = this.GetComponentInChildren<UnitHighlight>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ToggleHighlightUnit(bool isHighlighted)
        {
            if (isHighlighted && OnUnitHighlighted != null)
            {
                OnUnitHighlighted(this);
            }

            _unitHighlight.ToggleHighlight(isHighlighted);
        }

        public void ToggleSelectUnit(bool isSelected)
        {
            if (isSelected && OnUnitSelected != null)
            {
                OnUnitSelected(this);
            }

            _unitHighlight.ToggleSelection(isSelected);
        }
    }
}